package examples.lesson8;

import examples.lesson7.Person;

public class Lesson8 {
    public static void main(String[] args) {
//        Person person = new Person("Ainis", "Skabs");
//        person.displayInfo();
//        System.out.println();

        Employee employee = new Employee("Ainis", "Skabs", "Stormnet");
        employee.displayInfo();
        employee.getCompany();
        System.out.println();

        Person person2 = new Employee("1", "2", "3");
//        person2.sayHi();
//        person2.getCompany(); Так как объект базового класса, функционал дочернего не доступен
    }
}
