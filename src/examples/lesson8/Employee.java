package examples.lesson8;

import examples.lesson7.Person;

public class Employee extends Person {

    private String company;

    public Employee(String name, String secondName, String company) {
        super(name, secondName);
        this.company = company;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        if(company != null) {
            this.company = company;
        }
    }

    @Override
    public void displayInfo() {
        super.displayInfo();
        System.out.print(" " + company);
    }

    @Override
    public void sayHi() {
        System.out.println("Hi from " + name + " from " + company);
    }
}
