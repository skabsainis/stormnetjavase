package examples.lesson8;

import javax.swing.*;

public class MethodsOverview {

    private String name;

    public static void main(String[] args) {
        MethodsOverview lesson = new MethodsOverview();
        lesson.test1();

        int sum = lesson.getSum9();
        System.out.println(sum);

        sum = lesson.getSum(5);
        System.out.println(sum);

        lesson.test3(new int[]{1, 2}, "First", "Second");

        System.out.println("\n\n");

        lesson.countDown(5);
    }

    void test1() {
        test2();
    }

    // без возвращаемого значения
    private void test2() {
        System.out.println("test2");
    }

    // С возвращаемым значением
    private int getSum9() {
        int sum = 0;
        for (int i = 0; i < 10; i++) {
            sum += i;
        }
        return sum;
    }

    // Метод с параметром
    private int getSum(int count) {
        int sum = 0;
        for (int i = 1; i <= count; i++) {
            sum += i;
        }
        return sum;
    }

    private void test3(int[] sum, String... strings) {
        System.out.println(strings.length);
    }

    private void countDown(int tick) {
        System.out.println(tick);
        if (tick > 1) {
            countDown(--tick);
        }
    }
}
