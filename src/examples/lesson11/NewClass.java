package examples.lesson11;

public class NewClass {

    private int year = 2;
    private int age = 4;
    private String name;

    public NewClass() { }

    public NewClass(int year, int age, String name) {
        this.year = year;
        this.age = age;
        this.name = name;
    }

    @Override
        public int hashCode() {
            int hash = 3;
            // 2 + 4 == 4 + 2
            hash = 31 * hash + year;
            hash = 31 * hash + age;
            hash = 31 * hash + name.hashCode();

        // year = 2 age = 4
        // 3*3 + 2 = 11
        // 3 * 11 + 4 = 37

        // year = 4 age = 2
        // 3 * 3 + 4 = 13
        // 3 * 13 + 2 = 41

        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof NewClass)) {
            return false;
        }
        if(obj == null) {
            return false;
        }
        if(this == obj) {
            return true;
        }
        NewClass classObj = (NewClass) obj;
        return year == classObj.year && age == classObj.age
                && name.equals(classObj.name);
    }
}
