package examples.lesson11;

import com.sun.xml.internal.ws.util.StringUtils;

public class Main {
    public static void main(String[] args) {
        NewClass newClass = new NewClass(1, 2000, "hi");
        System.out.println(newClass.hashCode());

        NewClass newClass1 = new NewClass(1, 2000, "hi");
        System.out.println(newClass1.hashCode());

        boolean flag = 4 == 2;
        boolean flag2 = newClass == newClass1;
        System.out.println(flag2);

        newClass1 = newClass;
        flag2 = newClass == newClass1; // нельзя, сравниваются просто ссылки на объекты
        System.out.println(flag2);

        String s1 = new String("qqqq");
        String s2 = new String("qqqq");
        // Посмотреть intern()

        System.out.println("Строки через значения ссылок: " + (s1 == s2));

        System.out.println("Строки через equals: " + s1.equals(s2));

        System.out.println("Объекты через equals: " + newClass.equals(newClass1));

        newClass1 = new NewClass(1, 2000, "hi");

        System.out.println("Объекты через equals: " + newClass.equals(newClass1));

        System.out.println(newClass.getClass());
        System.out.println(NewClass.class.getSimpleName());
    }
}
