package examples.lesson10;

public class Lesson10 {

    public static void main(String[] args) throws InterruptedException {
        Tank tank = new Tank();

        Object obj = new Tank();
        Unit unit1 = new Tank();

        ((Tank) obj).displayInfo();

        unit1.go();
        tank.go();

        Soldier onHorse = new Cavalry();
        onHorse.go();
        if (onHorse instanceof Cavalry) {
            ((Cavalry) onHorse).climbUp();
        }

        Soldier soldier = new Soldier();
        soldier.level = SoldierLevel.LEVEL1;
        Soldier soldier2 = new Soldier();
        soldier2.level = SoldierLevel.LEVEL1;
        Soldier soldier3 = new Soldier();
        soldier3.level = SoldierLevel.LEVEL3;
        soldier.lockedListener = new ILockedListener() {
            @Override
            public void locked() {
                System.out.println("Чувака захватили");
            }
        };
        Unit[] army = new Unit[]{
                new Tank(),
                soldier,
                new Soldier(),
                new Soldier(),
                new Cavalry()
        };

        System.out.println("==========================================");

        for (Unit unit : army) {
            unit.go();
            if (unit instanceof Cavalry) {
                ((Cavalry) unit).climbUp();
            }

            if(unit instanceof Soldier && ((Soldier)unit).level != null) {
                boolean isLevel1 = ((Soldier)unit).level == SoldierLevel.LEVEL1;
                switch (((Soldier)unit).level) {
                    case LEVEL1:
                        System.out.println("Мы нашли рядового");
                        break;
                    case LEVEL2:
                        System.out.println("Мы нашли ефрейтор");
                        break;
                    case LEVEL3:
                        System.out.println("Мы нашли сержант");
                        break;
                }
            }
        }

        System.out.println("==================Interface==================");

        IShooter shooter = new Tank();
        shooter.shoot();
        ((Unit)shooter).go();

        System.out.println("Идет война...");

        Thread.sleep(3000);

        army[1].lockUnit();

        System.out.println(soldier3.toString());
    }
}
