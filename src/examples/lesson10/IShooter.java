package examples.lesson10;

public interface IShooter extends IRunning{

    void shoot();

    @Override
    default void run() {

    }

    default void perezaryadka() {
        System.out.println("Перезаряжаюсь");
    }
}

interface IRunning {
    default void run() {
        System.out.println("run");
    }
}

interface Second {

}


