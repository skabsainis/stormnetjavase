package examples.lesson10;

public class Tank extends Unit implements IShooter {

    public void displayInfo() {
        System.out.println("I'm tank");
    }

    @Override
    public void go() {
        System.out.println("Move Tank");
    }

    @Override
    public void shoot() {
        System.out.println("Tank shoot");
    }
}
