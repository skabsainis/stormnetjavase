package examples.lesson10;

public abstract class Unit {

    protected ILockedListener lockedListener;

    public void setLockedListener(ILockedListener lockedListener) {
        this.lockedListener = lockedListener;
    }

    public void lockUnit() {
        lockedListener.locked();
    }

    public abstract void go();
}
