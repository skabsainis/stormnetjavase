package examples.lesson10;

public class Soldier extends Unit implements IShooter {

    public boolean isLocked;

    public SoldierLevel level;

    @Override
    public void go() {
        System.out.println("Move Soldier");
    }

    @Override
    public void shoot() {
        System.out.println("Soldier shoot");
    }

    @Override
    public void perezaryadka() {
        System.out.println("Достаю патроны и перезаряжаюсь");
    }

    @Override
    public String toString() {
        return "Я есть робот";
    }
}
