package examples.lesson7;

public abstract class Person {

    protected String name;
    protected String secondName;

    // Константы с обязательной инициализацией
    private final static String prefixMen = "Mr.";

    public Person() {
        //Путстой конструктор
    }

    public Person(String name, String secondName) {
        this.name = name;
        this.secondName = secondName;
    }

    public String getFullName() {
        return name + " " + secondName;
    }

    public static int getDrinkAge(String country) {
        switch (country) {
            case "Belarus":
                return 18;
            default: return 21;
        }
    }

    public void displayInfo() {
        System.out.print(name + " " + secondName);
    }

    public abstract void sayHi();
}
