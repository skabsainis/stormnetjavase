package examples.lesson7;

class AccountDep {

    private String name;
    // Package-private
    String secondName;
    int age;

    String s1 = "a";
    String s2 = "a".intern(); // почитайте что это такое

    int i; // 0

    {
        // блок инициализации
        name = "qwe";
        secondName = "rty";
        age = 18;
        // Тоже самое что и конструктор без параметров
    }

    static {
        // статический блок инициализации
    }

    AccountDep() {
        Person person;
        Person person2;

//        person = new Person("Ainis", "Skabs");
//        person2 = new Person("qwe", "rty");
        int age = Person.getDrinkAge("Belarus");

//        printPerson(person);
//        printPerson(person2);
    }

    public AccountDep(String name, String secondName) {
        this.name = name;
        this.secondName = secondName;
    }

    public AccountDep(String name, String secondName, int age) {
        this(name, secondName);
        this.age = age;
    }

    // методы
    public static void main(String[] args) {
        String s = "qwe";
        for (int i = 0; i < 1000; i++) {
            s = s.concat("plus");
        }
    }

    public void printPerson(Person personToPrint) {
        System.out.println("Откат для " + personToPrint.getFullName());
    }
}
