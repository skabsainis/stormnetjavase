package examples.lesson9;

public class Student {
    public String name;
    public int course;
    public int[] marks;

    public Student() {
        name = "student";
        course = 1;
    }

    public Student(String name, int level) {
        this.name = name;
        course = level;
    }

    public void printInfo() {
        System.out.println(name + "  " + course);
    }

    public float getAverage() {
        int sum = 0;
        for (int i = 0; i < marks.length; i++) {
            sum += marks[i];
        }
        return (float)sum/marks.length;
    }
}
