package examples.lesson9;

public class Main {

    static Student skabs = new Student();

    public static void main(String[] args) {
        Student ivanov = new Student();
        ivanov.name = "Ivanov";
        ivanov.course = 1;
        // Создали объект и проинициализировали его поля

        Student petrov = new Student();
        petrov.name = "Petrov";
        petrov.course = 5;

        System.out.println(ivanov.name + " " + ivanov.course);
        System.out.println(petrov.name + " " + petrov.course);

        //Создаем пустую переменную
        System.out.println("Вывод ссылок");
        System.out.println(ivanov);
        System.out.println(skabs);

        petrov = ivanov;

        System.out.println("Задание 3");
        System.out.println(ivanov.name + " " + ivanov.course + " " + ivanov);
        System.out.println(petrov.name + " " + petrov.course + " " + petrov);

        ivanov.name = "Ivanov2";
        ivanov.course = 5;

        System.out.println("после изменения Ivanov");
        ivanov.printInfo();
        System.out.println(petrov.name + " " + petrov.course);

        System.out.println("Задача 5");
        ivanov.marks = new int[]{4, 5, 5, 5};

        System.out.println(ivanov.getAverage());
        System.out.println(petrov.getAverage());

        petrov = new Student();
        petrov.marks = new int[] {2, 3, 2};

        System.out.println("Для Петрова создали новый объект");
        System.out.println(ivanov.name + " " + ivanov.getAverage());
        System.out.println(petrov.name + " " + petrov.getAverage());

        System.out.println("Задача 8");
        Student sidorov = new Student("Sidorov", 3);
        sidorov.printInfo();
    }
}
