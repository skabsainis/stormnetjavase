package examples.lesson9.animals;

public class Animal {

    private int weight;
    private int age;
    private String name;

    public Animal() {
        System.out.println("Конструктор класса Animal");
        this.weight = 1000;
        age = 15;
        name = "Бизон";
    }

    public Animal(String name, int age, int weight) {
        System.out.println("Конструктор класса Animal с параметрами");
        this.name = name;
        this.age = age;
        this.weight = weight;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void displayInfo() {
        System.out.println(name + " " + age + " " + weight);
    }

    public void sayHi() {
        System.out.println("I don't speak animal");
    }

}
