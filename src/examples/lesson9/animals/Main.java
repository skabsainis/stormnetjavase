package examples.lesson9.animals;

public class Main {
    public static void main(String[] args) {
        Animal bison = new Animal();
        bison.displayInfo();

        Cat cat = new Cat();
        cat.displayInfo();

        Animal dog = new Animal("Дружок", 2, 3);
        dog.displayInfo();

        Cat cat2 = new Cat(11);
        cat2.displayInfo();

        Cat cat3 = new Cat("Барсик", 4, 3, 50);
        cat3.displayInfo();

        Dog dog1 = new Dog();
        cat3.sayHi();
        dog1.sayHi();

        bison.sayHi();
    }
}
