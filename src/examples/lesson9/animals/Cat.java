package examples.lesson9.animals;

public class Cat extends Animal {

    int caughtMice;

    public Cat() {
        System.out.println("Кнструктор класса Cat");
        caughtMice = 1;
    }

    public Cat(int caughtMice) {
        System.out.println("Кнструктор класса Cat с параметрами");
        this.caughtMice = caughtMice;
    }

    public Cat(String name, int age, int weight, int caughtMice) {
        super(name, age, weight);
        this.caughtMice = caughtMice;
        System.out.println("Кнструктор класса Cat со всеми параметрами");
    }

    public int getCaughtMice() {
        return caughtMice;
    }

    public void setCaughtMice(int caughtMice) {
        this.caughtMice = caughtMice;
    }

    @Override
    public void displayInfo() {
        System.out.println(getName() + " " + getAge() + " " + getWeight() + " " + caughtMice);
    }

    @Override
    public void sayHi() {
        System.out.println("Мяу");
    }
}
