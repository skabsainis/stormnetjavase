package examples.lesson9.animals;

public class Kitten extends Cat {

    private String eatTime;

    public String getEatTime() {
        return eatTime;
    }

    public void setEatTime(String eatTime) {
        this.eatTime = eatTime;
    }
}
