package examples;

public class Lesson6 {
    public static void main(String[] args) {

    }

    static void fromStringToCharArray() {
        String s = "Hello";
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            System.out.println(chars[i]);
        }
    }

    static void getCodeFromChar() {
        String s = "sjvn2kjfn4kjn6";
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char c = chars[i]; // Это символ
            int charCode = chars[i]; // Это код нашего символа
        }
    }

    static void generateRandomNumber() {
        System.out.println((int) (Math.random() * 10)); // от 0 до 10
        System.out.println((int) (Math.random() * 100)); // от 0 до 100
    }
}
